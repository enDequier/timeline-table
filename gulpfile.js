var gulp = require('gulp');
var browserify = require('gulp-browserify');
//var convertEncoding = require('gulp-convert-encoding');

// Basic usage
gulp.task('js', function() {
    // Single entry point to browserify
    gulp.src('./assets/js/source/app.js')
        .pipe(browserify({
            insertGlobals : true,
            debug : !gulp.env.production
        }))
        //.pipe(convertEncoding({to: 'windows-1251'}))

        .pipe(gulp.dest('./assets/js/dist/'))
});