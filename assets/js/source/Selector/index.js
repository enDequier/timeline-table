var $ = require('jquery');

var Selector = function () {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

Selector.prototype = {
    initialize: function (options) {
        /**
         * Must content:
         * $parent: {*|jQuery|HTMLElement} (Parent element where this selector will be work)
         *
         * May content:
         * barrier: {String} (ClassName of element that will be barrier)
         */
        this.options = options;

        /**
         * Move direction
         * @type {string}
         */
        this.direction = 'none';

        /**
         * use for fire events
         * @type {*|jQuery|HTMLElement}
         */
        this.events = $({}); //some events

        this.$el = $('<div class="selector_box"></div>');

        /**
         * Activation flag
         * @type {boolean}
         */
        this.active = false;

        /**
         * That's main property that's need to render
         * @type {number}
         */
        this.top = 0;
        this.height = 0;
        this.scrollTop = 0;


        /**
         * Coords to render
         * (getted from event object)
         * @type {number}
         */
        this.setYCoord(1, 0);
        this.setYCoord(2, 0);


        this._includeElementToParent();
        this._bindEvents();
        this._bindDomEvents();
    },

    /**
     * Append current selector element to Parent
     * @private
     */
    _includeElementToParent: function () {
        var $parent = this.options.$parent;
        if ($parent && $parent instanceof $) {
            this.options.$parent.prepend(this.$el);
        } else {
            console.error('Some problem with $parent element', this)
        }
    },


    /**
     * Some magic event subscriber ;)
     * @param event
     * @param callback
     */
    on: function (event, callback) {
        this.events.on(event, function () {
            var args = Array.prototype.slice.call(arguments, 1);
            callback.apply(null, args);
        });
    },


    setYCoord: function (number, value) {
        var windowScrollTop = $(window).scrollTop();
        var parentScrollTop = this.$el.parent().scrollTop();
        this['y' + number] = value + windowScrollTop + parentScrollTop;
    },

    getYCoord: function (number) {
        return this['y' + number];
    },

    /**
     * Add or remove no-select class tp body
     * class need to content "user-select: none" property
     * it's need for fix some selection problem
     * @param bool
     */
    setDomSelectable: function (bool) {
        $('body')[bool ? 'addClass' : 'removeClass']('no-select');
    },

    _bindEvents: function () {
    },

    /**
     * Mouse down handler
     * @param e
     * @private
     */
    _mouseDown: function (e) {
        var bool = true;
        // Check if we have barriers
        if (this.options.barrier) {
            // That's right! We have. We need to check if target is barrier.
            // So we check target and targets parent. If it's barrier - set flag to false
            if (e.target && ($(e.target).hasClass(this.options.barrier) || $(e.target).parents('.' + this.options.barrier).length)) {
                bool = false;
            }
        }

        //test flag
        if (bool) {
            this.active = true;
            this.setDomSelectable(this.active);

            //this.x1 = e.clientX; //don't need becouse we use only vertical line
            //this.x2 = this.x1; //don't need becouse we use only vertical line

            this.setYCoord(1, e.clientY);
            this.setYCoord(2, e.clientY);
            this.updateProps();
            this.$el.show();
        }
    },

    /**
     * Mouse up handler
     * @param e
     * @private
     */
    _mouseUp: function (e) {
        this.$el.hide();
        this._fireSelected();
        this.active = false;
        this.setDomSelectable(this.active);
        this.clearDirection();
    },

    _hide: function () {
        this.$el.hide();
        this.active = false;
    },


    /**
     * Mouse move handler
     * @param e
     * @private
     */
    _mouseMove: function (e) {
        if (this.active) {
            //this.x2 = e.clientX; //don't use becouse we use only vertical orientation
            this.setYCoord(2, e.clientY);
            this.updateDirection();
            this.updateProps();
        }
    },

    /**
     * Update selector direction
     */
    updateDirection: function () {
        this.direction = (this.getYCoord(1) < this.getYCoord(2)) ? 'down' : 'up';
    },

    /**
     * Clear selector direction
     */
    clearDirection: function () {
        this.direction = 'none';
    },

    /**
     * Find entry: B ∈ (A1 : A2)
     * @param a1
     * @param a2
     * @param b
     * @returns {boolean}
     * @private
     */
    _checkEntry: function (a1, a2, b) {
        a1 = Math.floor(a1);
        a2 = Math.floor(a2);
        b = Math.floor(b);
        return (a1 < b && a2 > b);
    },


    /**
     * Find conflict between current position and
     * barrier position. If conflict is detected returns object else
     * return false.
     * @param position
     * @param barrierPosition
     * @returns {boolean || object}
     *
     */
    findConflict: function (position, barrierPosition) {
        var barrierTop = barrierPosition.top;
        var barrierBottom = barrierPosition.height + barrierTop;
        var selectedTop = position.top;
        var selectedBottom = position.height + selectedTop;
        var parentTop = parent.top;
        var parentBottom = parent.height + parentTop;
        var conflict = false;

        var entries = [
            this._checkEntry(barrierTop, barrierBottom, selectedTop),
            this._checkEntry(barrierTop, barrierBottom, selectedBottom),
            this._checkEntry(selectedTop, selectedBottom, barrierTop),
            this._checkEntry(selectedTop, selectedBottom, barrierBottom)
        ];

        conflict = entries.some(function (bool) {
            return bool == true;
        });

        if (conflict) {
            conflict = {
                top: barrierTop,
                bottom: barrierBottom
            };
        }

        return conflict;
    },
    /**
     * Returns conflict list
     * @param $barriers {jQuery} list of barriers
     * @param position {object} current position
     * @returns {Array}
     * @private
     */
    _getConflicts: function ($barriers, position) {
        var that = this;
        var parentTop = this.$el.parent().offset().top;
        var result = [];

        $barriers.each(function () {
            var conflict = that.findConflict(position, {
                top: $(this).offset().top - parentTop,
                height: $(this).height()
            });

            if (conflict) {
                conflict.$el = $(this);
                result.push(conflict);
            }
        });

        return result;
    },

    /**
     * Find conflicts
     * @param top {integer} current top
     * @param height {integer} current height
     * @returns {Array}
     */
    findConflicts: function (top, height, barrierClass) {
        barrierClass = barrierClass || this.options.barrier;
        var result = [];
        if (barrierClass) {
            var $barriers = this.$el.parent().find('.' + barrierClass);
            if ($barriers.length) {
                result = this._getConflicts($barriers, {
                    top: top,
                    height: height
                });
            }
        }
        return result;
    },


    _render: function (top, height) {
        this.height = $.isNumeric(height) ? height : this.height;
        this.top = $.isNumeric(top) ? top : this.top;
        this.$el.css({
            //left: x3 + 'px',        //don't use becouse we use only vertical orientation
            //width: x4 - x3 + 'px',  //don't use becouse we use only vertical orientation
            top: this.top + 'px',
            height: this.height + 'px'
        });
    },

    /**
     * Resolve conflicts
     * Find closest conflict block and
     * sticks to him
     * @param conflicts
     */
    resolveConflicts: function (conflicts) {
        var position = {
            top: false,
            height: false
        };

        var conflict = conflicts.reduce(function (c, el) {
            var result;
            if (this.direction == 'down') {
                result = (el.top <= c.top) ? el : c;
                position.height = result.top - this.top;

            }

            if (this.direction == 'up') {
                result = (el.top >= c.top) ? el : c;
                position.top = result.bottom;
                position.height = (this.top + this.height) - position.top;
            }

            return result
        }.bind(this), conflicts[0]);

        var top = $.isNumeric(position.top) ? position.top : this.top;
        var height = $.isNumeric(position.height) ? position.height : this.height;

        if (this.direction == 'up' && $.isNumeric(position.top)) {
            top += 1;
            height -= 1;
        } else {
            height -= 1;
        }



        this._render(top, height);
    },

    /**
     *  Calculate proportions
     */
    updateProps: function () {
        var parentOffset = this.options.$parent.offset();
        var parentHeight = this.options.$parent.height();

        //var x3 = Math.min(this.x1, this.x2);  //don't use becouse we use only vertical orientation
        //var x4 = Math.max(this.x1, this.x2);  //don't use becouse we use only vertical orientation
        var y3 = Math.min(this.getYCoord(1), this.getYCoord(2));
        var y4 = Math.max(this.getYCoord(1), this.getYCoord(2));

        /**
         * calculate top
         * @type {number}
         */
        var top = (y3 <= parentOffset.top) ? 0 : y3 - parentOffset.top;

        /**
         * calculate height
         * @type {number}
         */
        var height = ((this.top == top) && (this.top + (y4 - y3)) >= parentHeight) ? parentHeight - this.top : y4 - y3;
        height = ((this.top == top) && top == 0) ? this.getYCoord(1) - parentOffset.top : height;


        /**
         * conflicts section
         */
        var conflicts = this.findConflicts(top, height);
        if (conflicts.length == 0) {
            this._render(top, height)
        } else {
            var position = this.resolveConflicts(conflicts);
        }


    },

    /**
     * Change current scrollTop to real value
     */
    updateScrollTop: function () {
        this.scrollTop = $(document).scrollTop();
    },


    /**
     * Returns scroll direction
     * @returns {string}
     */
    getScrollDirection: function () {
        return (this.scrollTop > $(document).scrollTop()) ? 'up' : 'down';
    },

    /**
     * Fix props by scroll top offset
     * TODO make it work
     * @private
     */
    _scroll: function () {
        if (this.active) {
            var direction = this.getScrollDirection();
            this.updateScrollTop();
        }
    },

    /**
     * Returns true if current scrollTop is not equal to Documents scrollTop
     * @returns {boolean}
     */
    isScrollChange: function () {
        return this.scrollTop != $(document).scrollTop();
    },



    _bindDomEvents: function () {
        $(document).on('mousemove', this._mouseMove.bind(this));
        $(document).on('mouseleave', this._hide.bind(this));
        //$(document).scroll(this._scroll.bind(this));
        this.options.$parent.on('mousedown tochstart tap', this._mouseDown.bind(this));
        $(document).on('mouseup tochend', this._mouseUp.bind(this));
    },


    destroy: function () {
        this.$el.unbind().off();
        this.events.unbind().off();
        this.$el.remove();
    },

    /*
     EventsFires
     */

    _fireSelected: function () {
        if (this.active) {
            this.events.trigger('selected', {
                top: this.top,
                height: this.height
            });
        }
    }
};

module.exports = Selector;
