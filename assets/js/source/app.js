var $ = require('jquery');

        require('jquery-ui/resizable');
        require('jquery-ui/draggable');
        require('jquery-ui-touch-punch');



var TimeLineTable = require('./TimeLineTable');
window.t = new TimeLineTable({
    backgroundAlpha: 0.3,
    fluid: true,
    template: function (data) {
        return '<div> Чувак на кресле №' + data.chair + '</div>';
    },

    headerTemplate: function (data) {
        return data.name
    },

    headerSubCloneTemplate: function (data) {
        return 'Кресло №' + data.chair
    },

    headerCloneTemplate: function (data) {
        return data.name
    },
    $editForm: $('<form><input name="chair" type="text"/><input type="submit"/></form>')
}, $('.test-table'));



t.addColumn({group: ['max', 1],
    background: '#FACE8D',
    data: {
        name: 'Max',
        chair: 1
    },
    clone:['max']
});

t.addColumn({group: ['peet', 2],
    data: {
        name: 'Peet',
        chair: 2
    },
    clone: ['peet'],
    template: function (data) {
        return '<div> Пупырка на кресле №' + data.chair + '</div>';
    },

    coolRows: [{
        end: 1446461100000,
        height: 150,
        left: 0,
        start: 1446459300000,
        template: function (data) {
            return '<div> Ололо на кресле №' + data.chair + '</div>';
        },
        top: 375,
        disabled: true
    }],

    background: '#FAFACD'
});
t.addColumn({group: ['peet', 3],
    data: {
        chair: 3,
        name: 'Peet'
    },
    clone: ['peet'],
    background: '#FAFACD'
});

t.addColumn({group: ['stuart', 2],
    data: {
        chair: 2,
        name: 'Stuart'
    },
    background: '#CA347B',
    clone: ['stuart']
});


t.addColumn({
    group: ['max', 3],
    background: '#FACE8D',
    data: {
        chair: 3,
        name: 'Max'
    },
    clone:['max']
});

t.addColumn({
    group: ['max', 5],
    background: '#FACE8D',
    data: {
        chair: 5,
        name: 'Max'
    },
    clone:['max']
});



$('input[name="step"]').on('change', function () {
    var val = $(this).val();
    if ($.isNumeric(val)) {
        t.setCellStep(val * 60000);
    }
});