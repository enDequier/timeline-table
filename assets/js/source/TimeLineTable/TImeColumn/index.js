var $ = require('jquery');
var Rows = require('../Row');
var moment = require('moment');

var TimeColumn = function () {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

TimeColumn.prototype = {
    initialize: function (options, table) {
        this.options = options;
        this.table = table;
        this.props = {
            timecolumn: true
        };
        this.id = Date.now().toString(32);

        this._initElement();
        this._initRows();
        this._bindEvents();
    },


    _bindEvents: function () {
        this.table.on('table:update-cellStep', this._updateLineRows.bind(this))
    },

    _updateLineRows: function () {
        this.rows = {};
        this.$rowsContainer.empty();
        for (var i = 1; i <= this.options.rows; i += 1) {
            var time = this.options.start + (this.options.cellStep * i);
            this.addRow(time);
        }
        this.$rowsContainer.find('.timeline-row_content:first').prepend(this.wrapTime(this.options.start, ['timeline-time_row_first']));
    },

    _initRows: function () {
        this.rows = {};
        this._updateLineRows();
    },

    wrapTime: function (time, additionalClasses) {
        additionalClasses = additionalClasses || [];
        return '<div class="timeline-time_row '+ additionalClasses.join(' ') +'">' + moment(time).format('HH:mm') + '</div>';
    },


    addRow: function (time) {
        var row = new Rows.Row({}, this.options, this);
        row.setContent(this.wrapTime(time));
        var id = row.id;
        this.rows[id] = row;
        this.$rowsContainer.append(row.$el);
        return row;
    },

    _initElement: function () {
        this.$el = $('<div class="timeline-time_column"></div>');
        this.$rowsContainer = $('<div class="timeline-row_container"></div>');
        this.$el.append(this.$rowsContainer);
    },

    _bindDomEvents: function () {
    }
};

module.exports = TimeColumn;
