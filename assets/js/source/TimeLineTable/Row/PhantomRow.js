var $ = require('jquery');
var Row = require('./Row');

var PhantomRow = function() {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

PhantomRow.prototype = $.extend({}, Row.prototype, {
    _initElement: function () {
        var classes = ['timeline-row_phantom'];


        if (this.props.$el) {
            this.$el = this.props.$el;
            this.$el.addClass(classes.join(' ')).empty();
        } else {
            if (this.props.disabled == true) {
                classes.push('timeline-row-cool-disabled');
            }
            this.$el = $('<div class="timeline-row-cool '+ classes.join(' ') +'"></div>');
        }
        this.$content = $('<div class="timeline-row_content"></div>');
        this.$el.append(this.$content);
    },

    save: function () {
        return false;
    },

    initialize: function (props, options, column, parent) {
        this.parent = parent;

        props = $.extend({}, props, {
            disabled: true
        });

        var args = Array.prototype.slice.call(arguments);
        Row.prototype.initialize.apply(this, args);
        this.setTop(props.top);
        this.setHeight(props.height);
        this._watchParent();

    },

    _watchParent: function () {
        this.parent.on('coolRow:update-phantom-height', this.setHeight.bind(this));
        this.parent.on('coolRow:update-phantom-top', this.setTop.bind(this));
    }


});


module.exports = PhantomRow;






