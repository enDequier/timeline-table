var $ = require('jquery');
var moment = require('moment')


/**
 * convert hex to rgba
 * @param hex {String| '#FACE8D'}
 * @param a {float| 0.4}
 * @returns {string}
 */
function hex2rgba (hex, a) {
    hex = hex.replace(/#/g, '');
    var template = '(r, g, b, a)';
    var div = (hex.length > 3) ?  1 : 2;

    var r = hex.substring(0, 2 / div);
    var g = hex.substring(2 / div, 4 / div);
    var b = hex.substring(4 / div, 6 / div);

    if (div == 2) {
        r += r;
        g += g;
        b += b;
    }

    template = template
        .replace(/r/g, parseInt(r, 16))
        .replace(/g/g, parseInt(g, 16))
        .replace(/b/g, parseInt(b, 16))
        .replace(/a/g, a || 1);

    return 'rgba' + template;
}


var Row = function () {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

Row.prototype = {
    initialize: function (props, options, column) {
        this.props = props;
        this.column = column;
        this.id = Date.now().toString(32);
        this.options = options;
        this.timeout = undefined;

        this.events = $({});
        this._initElement();
        this._bindDomEvents();
        this._initDirection();
        this._initBackground();

    },


    /**
     * Clear timeout
     * @private
     */
    _clearTimeout: function () {
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
    },

    /**
     * Set start timestamp
     * @param number
     */
    setStart: function (number) {
        if ($.isNumeric(number)) {
            this.props.start = number;
        }
    },

    /**
     * Set end timestamp
     * @param number
     */
    setEnd: function (number) {
        if ($.isNumeric(number)) {
            this.props.end = number;
        }
    },


    /**
     * Update time by difference between DOM offset and Table time offset
     * @private
     */
    _updateTimeByDOMOffset: function () {
        var start = this.props.top;
        var end = start + this.props.height;
        this.setStart(this.column.getTimeByPosition(start));
        this.setEnd(this.column.getTimeByPosition(end));
    },


    /**
     * Setting top and trigger save event if saveFlag is true;
     * Else update time by dom offset
     * @param number {float}
     * @param saveFlag {boolean}
     */
    setTop: function (number, saveFlag) {
        if (typeof saveFlag == 'undefined') {
            saveFlag = true;
        }
        if ($.isNumeric(number)) {
            if (saveFlag) {
                this._updateSaveTimeout();
            }
            this.$el.css({top : number + 'px'});
            this.props.top = number;
            this.events.trigger('coolRow:update-phantom-top', number);
            this._updateTimeByDOMOffset();
            //this.setContent(moment(this.props.start).format('HH:mm') + ' - ' + moment(this.props.end).format('HH:mm'));
        }
    },

    /**
     * Setting height and trigger save event if saveFlag is true;
     * Else update time by dom offset
     * @param number {float}
     * @param saveFlag {boolean}
     */
    setHeight: function (number, saveFlag) {
        if (typeof saveFlag == 'undefined') {
            saveFlag = true;
        }
        if ($.isNumeric(number)) {
            if (saveFlag) {
                this._updateSaveTimeout();
            }
            this.$el.css({height : number + 'px'});
            this.props.height = number;
            this.events.trigger('coolRow:update-phantom-height', number);
            this._updateTimeByDOMOffset();
            //this.setContent(moment(this.props.start).format('HH:mm') + ' - ' + moment(this.props.end).format('HH:mm'));
        }
    },

    /**
     * Checks difference between rows props
     * @param props1 {object} props of row
     * @param props2 {object} props of row
     * @returns {boolean}
     * @private
     */
    _isEqualHeightAndTop: function (props1, props2) {
        var helper = function (value1 , value2) {
            value1 -= value1 % 1;
            value2 -= value2 % 1;
            var bool = false;
            for (var i = -1, max = 1; i <= max; i += 1) {
                if (value1 + i == value2) {
                    bool = true;
                    break;
                }
            }
            return bool
        };

        return (helper(props1.height, props2.height) && helper(props1.top, props2.top));
    },

    /**
     * update background of all coolRows in column
     * @private
     */
    _initBackground: function () {
        if (!this.isDisabled()) {
            var color = this.props.background || this.column.props.background;
            if (color) {
                if (color.indexOf('#') >= 0) {
                    color = hex2rgba(color, this.options.backgroundAlpha);
                    this.column.props.background = color;
                }
                this.$el.css('background', color);
            }
        }
    },

    _initDirection: function () {
        this.clientY = this.props.top;
        this.direction = 'none';
    },

    _updateDirection: function (e) {
        if (e) {
            if (this.clientY) {
                this.direction = (this.clientY < e.clientY) ? 'down' : 'up';
            }
            this.clientY = e.clientY
        }
    },

    /**
     * Now it's not usable
     * TODO fix this if it's will need
     * @returns {string|string|*}
     * @private
     */
    _getDirection: function () {
        return this.direction;
    },

    /**
     * Update timeout. When time is gone it will fire save event
     * @private
     */
    _updateSaveTimeout: function () {
        this._clearTimeout();
        this.timeout = setTimeout(function () {
            this.save()
        }.bind(this), this.props.saveTime || 1500);
    },

    /**
     * Fire save event
     * @private
     */
    save: function () {
        this._fireSaveEvent()
    },


    /**
     * Remove coolRow and fire this event
     */
    remove: function () {
        this._fireRemoveEvent();
        this.column.removeCoolRow(this.id);
    },


    /**
     * Some magic event subscriber ;)
     * @param event
     * @param callback
     */
    on: function (event, callback) {
        this.events.on(event, function () {
            var args = Array.prototype.slice.call(arguments, 1);
            callback.apply(null, args);
        });
    },

    isDisabled: function () {
        return (this.props.disabled == true);
    },

    _initElement: function () {
        this.$el = $('<div class="timeline-row"></div>');
        this.$content = $('<div class="timeline-row_content"></div>');
        this.$el.append(this.$content);
        this.updateHeight(this.options.cellHeight);
    },

    setContent: function (string) {
        this.$content.html(string);
    },

    updateHeight: function (height) {
        this.$el.css({
            height: height + 'px'
        });
    },

    updateWidth: function (width) {
        this.$el.css({
            width: width + 'px'
        })
    },

    /**
     * correct position by step
     * @param value {float} (top or bottom)
     * @returns {*}
     * @private
     */
    _correctPositionByStep: function (value) {
        return this.column._correctPositionByStep(value);
    },

    /**
     * Update position by step
     * @param props
     * @returns {*}
     */
    updatePositionByStep: function (props) {
        props = props || this.props;
        props.top = this._correctPositionByStep(props.top);
        props.height = this._correctPositionByStep(props.height);
        return props;
    },

    _bindDomEvents: function () {

    },

    /**
     * Fire edit event on table and this element
     * @private
     */
    _fireEditEvent: function () {
        this._fireEvent('edit')
    },

    /**
     * Fire save event and this element
     * @private
     */
    _fireSaveEvent: function () {
        this._fireEvent('save')
    },

    /**
     * Fire remove event and this element
     * @private
     */
    _fireRemoveEvent: function () {
        this._fireEvent('remove')
    },

    /**
     * Fire remove add and this element
     * @private
     */
    _fireAddEvent: function () {
        this._fireEvent('add');
    },


    _fireEvent: function (name) {
        this.column.table.events.trigger('table:'+ name +'-coolRow', this);
        this.column.events.trigger('column:'+ name +'-coolRow', this);
        this.events.trigger('coolRow:'+ name, this);
    },




    destroy: function () {
        this.events.unbind();
        this.events.off();
        this.events.remove();
        this.$el.unbind();
        this.$el.remove();
    }
};

module.exports = Row;
