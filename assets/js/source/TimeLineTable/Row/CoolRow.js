var $ = require('jquery');
var Row = require('./Row');
var moment = require('moment')

var CoolRow = function () {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

var PhantomRow = require('./PhantomRow');




/**
 * available props:
 * {
 *      data: may be whatever you want
 *      start: milliseconds (event start at)
 *      end: milliseconds (event end at)
 *      top: float (position from top of container in px)
 *      height: float (height of block in px)
 *      disabled: bool (Opportunity to edit block)
 * }
 */

/**
 * Main Methods
 * setContent(string) arguments: string(html string) -> update content of row to this string
 * setData(object) arguments: object -> update param this.props.data and setContent(this.props.template(this.props.data)) if have this.props.template function
 * remove() arguments: null -> remove this row and all phantoms
 * addPhantom(column)
 * eachPhantoms(callback)
 * removePhantoms()
 * save() -> fire save event
 * on(eventName, callback) -> event subscriber
 * isDisabled() -> return bool
 * setTop(number) -> update top of element and update time
 * setHeight(number) -> update height of element and update time
 */

CoolRow.prototype = $.extend({}, Row.prototype, {
    initialize: function () {
        var args = Array.prototype.slice.call(arguments);
        Row.prototype.initialize.apply(this, args);

        this._updateTemplate();
        this._initRange();
        this._makeResizable();
        this._makeDraggable();


        setTimeout(function () {
            this._updateRange();
            this.setData({});
        }.bind(this));
    },

    /**
     * Set data in row and update content after that
     * if has template function
     * @param data
     */
    setData: function (data) {
        $.extend(this.props.data, data);
        if (typeof this.props.template == 'function') {
            this.setContent(this.props.template(this.props.data, this));
        }
    },

    /**
     * Update template function
     * @private
     */
    _updateTemplate: function () {
        if (this.column.props.template && !this.props.template) {
            this.props.template = this.column.props.template;
        }
    },

    /**
     * Init range element and append it to this main element
     * @private
     */
    _initRange: function () {
        this.$range = $('<div class="timeline-row-cool-range"></div>');
        this.$el.prepend(this.$range);
    },

    /**
     * Update range
     * @param start {int|milliseconds}
     * @param end {int|milliseconds}
     * @private
     */
    _updateRange: function (start, end) {
        start = start || moment(this.props.start).format('HH:mm');
        end = end || moment(this.props.end).format('HH:mm');
        this.$range.html(start + ' - ' + end);
    },

    /**
     * Remove all phantoms
     */
    removePhantoms: function () {
        for (var key in this.phantoms) {
            this.phantoms[key].destroy();
            delete this.phantoms[key];
        }
    },

    /**
     * Init phantoms
     * @private
     */
    _initPhantoms: function () {
        if (!this.isDisabled()) {
            this.column.eachColumnsInGroups(function (column) {
                if (!this.column.isMe(column)) {
                    this.addPhantom(column);
                }
            }.bind(this));
        }
    },


    /**
     * Init main elements
     * @private
     */
    _initElement: function () {
        this.phantoms = {};
        var classes = [];

        if (this.isDisabled()) {
            classes.push('timeline-row-cool-disabled');
        }

        this.$edit = $('<div class="timeline-table-edit_row timeline-table-row_icons"><span class="timeline-table-edit_row_button">&#9998;</span></div>');
        this.$remove = $('<div class="timeline-table-romove_row timeline-table-row_icons">&times;</div>');
        this.$el = $('<div class="timeline-row-cool '+ classes.join(' ') +'"></div>');
        this.$content = $('<div class="timeline-row_content"></div>');
        this.$el.append(this.$remove);
        this.$el.append(this.$edit);
        this.$el.append(this.$content);
    },

    /**
     * Prepare phantom props
     * @param column
     * @private
     */
    _preparePhantomProps: function (column) {
        var props = $.extend({}, this.props);
        //if (this.column.phantomIsClone(column)) {
        //    props.$el = this.$el.clone();
        //}
        return props;
    },

    /**
     * Call callback to all phantoms
     * @param callback
     */
    eachPhantoms: function (callback) {
        if (typeof callback == 'function') {
            for (var key in this.phantoms) {
                callback(this.phantoms[key]);
            }
        }
    },

    /**
     * Check if already has phantom in this column
     * @param column
     * @returns {boolean}
     * @private
     */
    _alreadyHasPhantomInColumn: function (column) {
        var bool = false;
        this.eachPhantoms(function (phantom) {
            if (column.id == phantom.column.id) {
                bool = true;
            }
        });
        return bool;
    },


    /**
     * Add phantom to this phantom collection
     * @param column
     * @private
     */
    _addPhantom: function (column) {
        if (!this._alreadyHasPhantomInColumn(column)) {
            var phantom = new PhantomRow(this._preparePhantomProps(column), this.options, column, this);
            this.phantoms[phantom.id] = phantom;
            phantom.$el.hide();
            column.$el.append(phantom.$el);
            setTimeout(function () {
                phantom.$el.slideDown(400);
            });
        }
    },


    /**
     * Clone element to clonegroup
     */
    cloneMe: function () {
        // find container
        var $container = this.column.$el.parents('.timeline-column-clone_container');
        // move this element from column to column clone container
        $container.append(this.$el);
        $.extend(this.props, {
            left: 0
        });
        // add phantoms for each columns in group
        this.column.eachColumnsInGroups(function (column) {
            this._addPhantom(column)
        }.bind(this));
    },


    /**
     * Make some checks to add phantom
     * @param column
     */
    addPhantom: function (column) {
        // check if column has this column groups
        if (column && column.hasGroup(this.column.props.group) && !this.isDisabled()) {
            // check is clone
            if (!this.column.phantomIsClone(column)) {
                // if not - add
                this._addPhantom(column);
            } else {
                //else clone
                setTimeout(function () {
                    this.cloneMe();
                }.bind(this));
                // update some props (left, width, e.t.c)
                this.updateOtherProps();
            }

        }

    },

    /**
     * Update some props
     * this list may be scalable
     */
    updateOtherProps: function () {
        var css = {};
        if (this.props.width) {
            $.extend(css, {
                width: this.props.width + 'px'
            })
        }

        if (this.props.left) {
            $.extend(css, {
                left: this.props.left + 'px'
            })
        }

        this.$el.css(css);
    },


    /**
     * Check conflict with container
     * @returns {Array}
     * @private
     */
    _findConflictWithContainer: function () {
        // get real position of container
        var containerProps = this._getContainerZeroProps();
        var result = [];
        for (var i = 0, max = 2; i < max; i ++) {
            // find conflict with top and height (real bottom) of the container
            var prop = containerProps[ i ? 'top' : 'height'];
            var conflict = this.column.selector.findConflict(this.props, {
                top: prop,
                height: 0
            });
            if (conflict) {
                result.push(conflict);
            }
        }
        return result;
    },

    /**
     * Check row
     * return false if this is the same row
     * return false if it row is disabled and from another column
     * else return true
     * (Needed for start to find conflicts with this row, if returns false then we dont do it)
     * @param row
     * @returns {boolean}
     * @private
     */
    _checkConflictsProperties: function (row) {
        var bool = true;

        if (bool) {
            bool = (row.id != this.id);
        }

        if (bool && this.column.hasGroup()) {
            if (row.isDisabled()) {
                bool = this.column.id == row.column.id;
            }
        }

        return bool;
    },

    /**
     * Find conflicts between this and other rows in this column
     * @param position {object} current position
     * @returns {Array}
     */
    findConflicts: function (position) {
        position = position || this.props;
        var rows = this.column.getCoolRowsInGroups();
        var result = [];
        //for each rows
        for (var key in rows) {
            var row = rows[key];
            //if it's not this row
            if (this._checkConflictsProperties(row)) {
                if (this._isEqualHeightAndTop(position, row.props)) {
                    result.push({
                        top: row.props.top,
                        bottom: row.props.top + row.props.height
                    });
                }
                //find conflict with finded row
                var conflict = this.column.selector.findConflict(position, row.props);
                //if conflict exist
                if (conflict) {
                    //collect it
                    conflict.$el = row.$el;
                    result.push(conflict);
                }
            }
        }

        return result.concat(this._findConflictWithContainer());  //add conflicts with container
    },

    /**
     * Resolving resizable conflict and return actual position to resolve it
     * @param conflicts {Array} of conflicts object
     * @param direction {String} can be 'up' or 'down'
     * @param top {float} plugins top
     * @param height {float} plugins height
     * @returns {{top: *, height: *}}
     */
    resolveResizableConflicts: function (conflicts, direction, top, height) {
        var props = this.resolveConflicts(conflicts, direction, top, height);
        var answer = {
            top: $.isNumeric(props.top) ? props.top : top,
            height: $.isNumeric(props.height) ? props.height : height
        };

        //some trick to fix height problem when resize (Why it didn't fixed by author of jQuery resizable plugin?)
        //check conflict top < current top need to correct to actual height
        if (props.conflict.top < this.props.top) {
            answer.height = (this.props.height + this.props.top) - answer.top;
        }
        return answer;
    },

    /**
     * Find and resolve closest conflict
     * @param conflicts {Array} of conflicts object
     * @param direction {String}
     * @param top {float}
     * @param height {float}
     * @returns {{top: float, height: float}}
     */
    resolveConflicts: function (conflicts, direction, top, height) {
        var props = {
            top: false,
            height: false
        };

        props.conflict = conflicts.reduce(function (c, el) {
            var result;
            if (direction == 'down') {
                result = (el.top <= c.top) ? el : c;
                props.height = result.top - top;
            }

            if (direction == 'up') {
                result = (el.top >= c.top) ? el : c;
                props.top = result.bottom;
                props.height = (top + height) - props.top;
            }

            return result;
        }.bind(this), conflicts[0]);

        return props;
    },


    /**
     * Its update resize direction
     * @param e
     * @private
     */
    _onStartResize: function (e) {
        this.resizeDirection = $(e.toElement).hasClass('ui-resizable-n') ? 'up' : 'down';
        this.updateOtherProps()
    },

    /**
     * Check and fix props
     * Find and resolve conflicts
     * @param e
     * @param ui
     * @private
     */
    _onResize: function (e, ui) {
        this._updateDirection(e);
        var top = this._correctPositionByStep(ui.position.top);
        var height = this._correctPositionByStep(ui.size.height + ui.position.top) - top;

        var conflicts = this.findConflicts({
            top: top,
            height: height
        });


        //fix problem with north direction
        if (top != this.props.top && !conflicts.length) {
            var diff = this.props.top - top;
            height = diff + this.props.height;
        }

        if (conflicts && conflicts.length) {
            var resolve = this.resolveResizableConflicts(conflicts,  this.resizeDirection, top, height);
            top = resolve.top;
            height = resolve.height;
        }

        this.setTop(top);
        this.setHeight(height);
        this.updateOtherProps()
        this._updateRange();
    },

    /**
     * Returns real top prop in container
     * @returns {number}
     * @private
     */
    _getElementTopInContainer: function () {
        var $container = this.$el.parents('.timeline-column');
        return this.$el.offset().top - $container.offset().top;
    },

    /**
     * Returns real size of container
     * @returns {{top: number, bottom: number}}
     * @private
     */
    _getContainerZeroProps: function () {
        var $container = this.column.$el;
        var diff = $container.offset().top;
        return {
            top: 0,
            bottom: $container.height() - diff,
            height: $container.height()
        }

    },


    /**
     * It function trying to find place element beside conflicts element
     * @param top
     * @param height
     * @returns {boolean}
     * @private
     */
    _tryingToDropBeside: function (top, height) {
        var result = false;
        var containerZeroOffset = this._getContainerZeroProps();
        var _pTop = containerZeroOffset.top;
        var _pBottom = containerZeroOffset.bottom;
        for (var i = 20; i >= -20; i -= 1) {
            var _top = top + i;
            var _bottom = _top + height;
            var conflicts = this.findConflicts({
                top: _top,
                height: height
            });

            if (!conflicts.length && _pTop <= _top && _bottom <= _pBottom) {
                result = top + i;
                break;
            }
        }

        return result;
    },

    /**
     * It function trying to find place beside conflict element
     * @param top
     * @param height
     * @param conflicts
     * @returns {boolean}
     * @private
     */
    _tryingToDropBesideOne: function (top, height, conflicts) {
        var conflict = conflicts[0];
        var result = false;
        var bool = conflict.top < top;
        var containerZeroOffset = this._getContainerZeroProps();
        var _pTop = containerZeroOffset.top;
        var _pBottom = containerZeroOffset.bottom;
            for (var i = 0; bool ? i <= 50 : i >= -50; bool ? i += 1 : i -= 1) {
                var _top = top + i;
                var _bottom = _top + height;
                var _conflicts = this.findConflicts({
                    top: _top,
                    height: height
                });

                if (!_conflicts.length && _pTop <= _top && _bottom <= _pBottom) {
                    result = top + i;
                    break;
                }
            }
        return result;
    },



    /**
     * On drop handler
     * Check and fix props, find and resolve conflicts
     * @param e
     * @param ui
     * @private
     */
    _onDrop: function (e, ui) {
        this._updateDirection(e);
        var top = this._correctPositionByStep(ui.position.top);
        var height = this._correctPositionByStep(this.props.height + ui.position.top) - top;

        var conflicts = this.findConflicts({
            top: top,
            height: height
        });

        if (conflicts && conflicts.length) {
            top =  this[(conflicts.length == 1) ? '_tryingToDropBesideOne' : '_tryingToDropBeside'](top, height, conflicts) ||  this.props.top;
            height = this.props.height;
        }

        this.setTop(top);
        this.setHeight(height);
        this.updateOtherProps()
        this._updateRange();
    },


    /**
     * Make element resizable;
     * Add resizable plugin to it element
     * @private
     */
    _makeResizable: function () {
        if (!this.isDisabled()) {
            this.$el.resizable({
                handles: "s, n",
                containment: "parent",
                resize: this._onResize.bind(this),
                start: this._onStartResize.bind(this),
                minHeight: this.options.minCellHeight
            });
        }
    },

    /**
     * Make it element active
     * @param bool
     * @private
     */
    _setActive: function (bool) {
        if (typeof bool == 'undefined') {
            bool = true;
        }
        var action = bool ? 'addClass' : 'removeClass';
        this.$el[action]('active').parent()[action]('timeline-column_active');
    },

    _bindDomEvents: function () {
        if (!this.isDisabled()) {
            this.$el.on('mouseenter', function (e) {
                this._updateDirection(e);
                this._setActive(true);
            }.bind(this));
            this.$el.on('mouseleave', function (e) {
                this._updateDirection(e);
                this._setActive(false);
            }.bind(this))
        }

        this.$remove.on('click tap', this.remove.bind(this));
        this.$edit.on('click', this._fireEditEvent.bind(this))
    },


    /**
     * Make element draggable
     * Add draggable plugin to this element
     * @private
     */
    _makeDraggable: function () {
        if (!this.isDisabled()) {
            this.$el.draggable({
                containment: "parent",
                stop: this._onDrop.bind(this),
                drag: function (e, ui) {
                    var top = ui.position.top;
                    var start = this.column.getTimeByPosition(top);
                    var end = this.column.getTimeByPosition(this.props.height + top);
                    this._updateRange(moment(start).format('HH:mm'), moment(end).format('HH:mm'));
                    this.updateOtherProps();
                    this.events.trigger('coolRow:update-phantom-top', top);
                }.bind(this)
            });
        }
    },

    render: function () {
        this.setHeight(this.props.height, false);
        this.setTop(this.props.top, false);
        this.updateOtherProps();
    },



    destroy: function () {
        this._setActive(false);
        this.$el.draggable("destroy");
        this.$el.resizable("destroy");
        this.$remove.off().unbind();
        this.$edit.off().unbind();
        this.removePhantoms();
        Row.prototype.destroy.call(this);
    }
});

module.exports = CoolRow;