var $ = require('jquery');
var Rows = require('../Row');
var Selector = require('../../Selector');

var Column = function () {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

Column.prototype = {
    initialize: function (props, options, table) {
        this.props = props;
        this.options = options;
        this.table = table;
        this.events = $({});
        this.id = Date.now().toString(32);

        this._updateTemplate();
        this._initHeader();
        this._initElement();
        this._initRows();
        this._initSelector();
        this._bindEvents();
        this._bindDomEvents();
    },

    _initHeader: function () {
        this.$header = $('<div class="timeline-column_header"></div>');
        this.setHeaderContent();
    },

    setHeaderContent: function (string) {
        if (!string) {
            if (this.isClone()) {
                if (typeof this.props.headerSubCloneTemplate == 'function') {
                    string = this.props.headerSubCloneTemplate(this.props.data, this);
                }
            } else {
                if (typeof this.props.headerTemplate == 'function') {
                    string = this.props.headerTemplate(this.props.data, this);
                }
            }
        }
        if (string) {
            this.$header.html(string);
        }
    },

    _updateTemplate: function () {
        if (this.options.template && !this.props.template) {
            this.props.template = this.options.template;
        }

        if (this.options.headerTemplate && !this.props.headerTemplate) {
            this.props.headerTemplate = this.options.headerTemplate;
        }

        if (this.options.headerCloneTemplate && !this.props.headerCloneTemplate) {
            this.props.headerCloneTemplate = this.options.headerCloneTemplate;
        }

        if (this.options.headerSubCloneTemplate && !this.props.headerSubCloneTemplate) {
            this.props.headerSubCloneTemplate = this.options.headerSubCloneTemplate;
        }
    },

    hasGroup: function (groups) {
        var bool = false;
        if (!groups) {
            bool = !!this.props.group;
        } else {
            this.props.group.forEach(function (groupName) {
                if (groups.indexOf(groupName) >= 0) {
                    bool = true
                }
            });
        }

        return bool;
    },

    /**
     * @return {}
     */
    getCoolRowsInGroups: function () {
        var result = {};

        this.eachRowsInGroups(function(coolRow) {
            result[coolRow.id] = coolRow;
        });

        return result;
    },

    /**
     * @param callback
     */
    eachColumnsInGroups: function(callback) {
        if (typeof callback != 'function') {
            throw 'Callback must be a function!';
        }

        var groups = this.props.group;
        var cols = {};

        if ($.isArray(groups) && groups.length) {
            var result = {};

            for (var groupKey in groups) {
                var group = this.table.groups[groups[groupKey]];

                for (var colKey in group) {
                    var column = group[colKey];

                    if (cols[colKey] !== true) {
                        callback(column, colKey);
                    }

                    cols[colKey] = true;
                }
            }
        }

        return result;
    },

    /**
     * @param callback
     */
    eachRowsInGroups: function(callback) {
        if (typeof callback != 'function') {
            throw 'Callback must be a function';
        }

        this.eachColumnsInGroups(function(col) {
            col.eachCoolRows(callback);
        });
    },

    getTimeByPosition: function (number) {
        return this.table.getTimeByPosition(number, this.$el.height());
    },

    getPositionByTime: function (time) {
        return this.table.getPositionByTime(time,  this.$el.height());
    },

    isMe: function (column) {
        return column.id == this.id
    },

    /**
     * Some magic event subscriber ;)
     * @param event
     * @param callback
     */
    on: function (event, callback) {
        this.events.on(event, function () {
            var args = Array.prototype.slice.call(arguments, 1);
            callback.apply(null, args);
        });
    },

    eachCoolRows: function (callback) {
        if (callback && typeof(callback) == 'function') {
            for (var key in this.coolRows) {
                callback(this.coolRows[key]);
            }
        }
    },


    _initSelector:function () {
        this.selector = new Selector({
            $parent: this.$el,
            barrier: 'timeline-row-cool'
        });
    },

    _updatePhantoms: function (column) {
        setTimeout(function () {
            this.eachRowsInGroups(function (coolRow) {
                coolRow.addPhantom(column);
            })
        }.bind(this));

    },

    _bindEvents: function () {
        this.selector.on('selected', this.addCoolRowBySelector.bind(this));
        this.table.on('table:update-cellStep', this._updateStep.bind(this));
        this.table.on('table:remove-coolRow', this.removeCoolRow.bind(this));
        this.table.on('table:add-column', this._updatePhantoms.bind(this));
    },



    /**
     * Remove coolRow from collection
     * @param id {string} coolRowId
     */
    removeCoolRow: function (id) {
        var row = this.coolRows[id];
        if (row) {
            row.destroy();
            delete this.coolRows[id];
        }
    },

    _updateLineRows: function () {
        this.rows = {};
        this.$rowsContainer.empty();
        for (var i = 1; i <= this.options.rows; i += 1) {
            this.addRow({
                background: 'white'
            });
        }
    },

    _updateCoolRows: function () {
        for (var key in this.coolRows) {
            var row = this.coolRows[key];
            var top = this.getPositionByTime(row.props.start);
            var bottom = this.getPositionByTime(row.props.end) - top;
            row.setTop(top, false);
            row.setHeight(bottom, false);
        }
    },

    _updateStep: function () {
        this._updateLineRows();
        this._updateCoolRows();
    },

    _initRows: function () {
        this.coolRows = {};
        this._updateLineRows();
    },

    addRow: function (props) {
        var row = new Rows.Row(props, this.options, this);
        var id = row.id;

        this.rows[id] = row;
        this.$rowsContainer.append(row.$el);

        return row;
    },

    phantomIsClone: function (anotherColumn) {
        var bool = false;
        if (this.props.clone && this.props.clone.length && anotherColumn.props.group && anotherColumn.props.group.length) {
            var clones = this.props.clone;
            var serchIn = anotherColumn.props.group;
            for (var i = 0, max = clones.length; i < max; i += 1) {
                if (serchIn.indexOf(clones[i]) >= 0) {
                    bool = true;
                    break;
                }
            }
        }
        return bool;
    },

    isClone: function () {
        return !!this.props.clone
    },

    _checkRowPropsPosition: function (coolRow) {
        return !!($.isNumeric(coolRow.props.top) && $.isNumeric(coolRow.props.height));
    },

    _checkRowPropsTime: function (coolRow) {
        return !!($.isNumeric(coolRow.props.start) && $.isNumeric(coolRow.props.end));
    },

    _checkRowTimeSize: function (coolRow) {
        var size = true;
        if (this.options.min) {
            var start = this.getTimeByPosition(coolRow.props.top);
            var end = this.getTimeByPosition(coolRow.props.top + coolRow.props.height);
            size = (end - start) >= this.options.min;
        }
        return size
    },

    _checkRowConflict: function (coolRow) {
        return coolRow.findConflicts().length == 0;
    },

    checkRow: function (coolRow) {
        var size = this._checkRowTimeSize(coolRow);
        var conflicts = this._checkRowConflict(coolRow);
        var position = this._checkRowPropsPosition(coolRow);
        var time = this._checkRowPropsTime(coolRow);
        var props = position || time;
        var result = [size, conflicts, props];
        var answer = {
            correct: result.indexOf(false) == -1,
            size: size,
            conflicts: conflicts,
            props: props,
            position: position,
            time: time
        };

        return answer;
    },


    _addCoolRow: function (coolRow, saveFlag) {
        saveFlag = (typeof saveFlag == 'undefined') ? true : saveFlag;
        coolRow.render();
        coolRow.$el.hide();
        var id = coolRow.id;
        this.coolRows[id] = coolRow;
        this.$el.append(coolRow.$el);
        coolRow.$el.slideDown(400);
        coolRow._initPhantoms();
        if (saveFlag) {
            this.events.trigger('column:add-coolRow', [this, coolRow]);
            coolRow.save();
            coolRow._fireAddEvent();
            coolRow._fireEditEvent();
        }
        return coolRow;
    },

    _tryingToFixRow: function (coolRow, checker) {
        if (!checker.position && checker.time) {
            var top = this.getPositionByTime(coolRow.props.start);
            var height = this.getPositionByTime(coolRow.props.end) - top;
            coolRow.props.top = top;
            coolRow.props.height = height;
        }

        if (!checker.size && checker.position) {
            var start = this.getTimeByPosition(coolRow.props.top);
            var end = this.options.min + start;
            var height = this.getPositionByTime(end) - coolRow.props.top;
            coolRow.setHeight(height, false);
        }
    },

    validateRow: function (coolRow) {
        var isCorrectRow = false;
        var checker = this.checkRow(coolRow);
        isCorrectRow = checker.correct;
        if (!isCorrectRow) {
            this._tryingToFixRow(coolRow, checker);
            isCorrectRow = this.checkRow(coolRow).correct;
        }
        return isCorrectRow;
    },


    /**
     * Correct row position by step in options if key step
     * is a number > 0.
     * @param value {number} it's height or top (position)
     * @returns {*}
     * @private
     */
    _correctPositionByStep: function (value) {
        //checks step in options
        if (this.options.step) {
            var time = this.getTimeByPosition(value);
            var step =  this.options.step;
            // looking for difference between time and needed time
            var diff = time % step;
            // if difference not equal zero
            if (diff && diff != 0) {
                // magic trick to find ceil way
                // for example:
                // we have step == 5;                || we have step == 5;
                // we have time == 7                 || we have time == 3
                // then 7 % 5 == 2 (it's our diff)   || then 3 % 5 == 3;
                // 5 / 2 == 2.5  (2.5 > 2)           || 5 / 3 == ~1.6 (1.6 < 2)
                // so 7 - 2 == 5. That's it!         || so 3 + (5 - 2) == 5. That's it!
                time = (step / diff > 2) ? time - diff : time + (step - diff);
                value = this.getPositionByTime(time);
            }

        }
        return value;
    },

    addCoolRowBySelector: function (props) {
        data = Rows.Row.prototype.updatePositionByStep.call(this, props);
        this.addCoolRow(props);
    },

    addCoolRow: function (props, saveFlag) {
        props.data = this.props.data || {};
        var coolRow = new Rows.CoolRow(props, this.options, this);
        if (this.validateRow(coolRow)) {
            return this._addCoolRow(coolRow, saveFlag);
        } else {
            coolRow.destroy();
        }
    },

    updateWidth: function (width) {
        width = width || this.options.cellWidth;
        if (this.isClone()) {
            width = Math.floor(this.options.cellWidth / this.$el.parent().find('.timeline-column').length - 0.5);
        }
        var css = {
            width: width + 'px'
        };
        this.$el.css(css);
        this.$header.css(css)
    },

    _initElement: function () {
        this.$el = $('<div class="timeline-column"></div>');
        this.$rowsContainer = $('<div class="timeline-row_container"></div>');
        this.updateWidth(this.options.cellWidth);
        this.$el.append(this.$rowsContainer);
    },

    remove: function () {
        this.eachCoolRows(function (coolRow) {
            this.removeCoolRow(coolRow.id)
        }.bind(this));
        this._fireRemove();
        this._destroy();
    },

    _destroy: function () {
        this.$el.remove();
        this.$el.unbind().off();
        this.$header.remove().unbind().off();
        this.selector.destroy();
    },

    _bindDomEvents: function () {

    },

    _fireRemove: function () {
        this._fireEvent('remove');
    },

    _fireEvent: function (name) {
        this.table.events.trigger('table:'+ name +'-column', this);
        this.events.trigger('column:'+ name, this);
    },
};

module.exports = Column;
