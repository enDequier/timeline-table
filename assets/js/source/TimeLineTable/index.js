var $ = require('jquery');
var Column = require('./Column');
var TimeColumn = require('./TimeColumn');
var moment = require('moment');

var mobileAndTabletcheck = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
};

var TimelineTable = function () {
    var args = Array.prototype.slice.call(arguments);
    this.initialize.apply(this, args);
};

TimelineTable.prototype = {
    initialize: function (options, $container) {
        this.$container = $container;
        this.columns = {};
        this.groups = {};
        this.events = $({});
        this.fluidTimeout = undefined;
        this._updateOptions(options);
        this._initElement();
        this._initColumnContainer();
        this._initTimeColumn();
        this._initHeader();
        this._initPointer();
        this._initColumns();
        this._bindDomEvents();
        this._bindEvents();
        this.$container.append(this.$el);
    },

    defaultOptions: {
        header: true, // show header or not
        date: +moment(moment().format('DD.MM.YY'), 'DD.MM.YY'),
        pointer: !mobileAndTabletcheck(),
        backgroundAlpha: 0.9, //default alpha for coolRows background
        //template: function (data, row) { /* returns dome structure that use data  */ },
        //headerTemplate: function (data, column) { /* returns dome structure that use data  */ },
        //headerCloneTemplate: function (data, column) { /* returns dome structure that use data  */ },
        //headerSubCloneTemplate: function (data, column) { /* returns dome structure that use data  */ },
        columns: 0,     // columns count
        fluid: false,
        cellHeight: 150,   // px
        cellWidth: 300,   // px
        min: 60000 * 30,  // minimal row time size create after click
        step: 5 * 60 * 1000, // action step (resize, drop, select);
        cellStep: 60000 * 30,   // mseconds
        start: +moment({hour: 12}),       // mseconds
        end: +moment({hour: 18}),   // mseconds
        minCellHeight: 150  //px calculate from options.min
    },

    setCellStep: function (cellStep) {
        if ($.isNumeric(cellStep)) {
            this.options.cellStep = cellStep;
            this._calculateRowsCount();
            this.events.trigger('table:update-cellStep')
        }
    },

    getTimeByPosition: function (number, height) {
        var time = 0;
        if ($.isNumeric(number)) {
            height = height || this.$columns.height();
            time = (this.options.end - this.options.start) * number / height;
        }
        return time + this.options.start;
    },


    _initHeader: function () {
        this.$header = $('<div class="timeline-header"></div>');
        if (this.options.header) {
            this.$el.prepend(this.$header);
        }
    },

    getPositionByTime: function (time, height) {
        var position = 0;
        if ($.isNumeric(time)) {
            height = height || this.$columns.height();
            time = time - this.options.start;
            position = height * time / (this.options.end - this.options.start);
        }
        return position;
    },

    /**
     * This function do what they called
     * @private
     */
    _calculateRowsCount: function () {
        this.options.rows = Math.ceil((this.options.end - this.options.start) / this.options.cellStep);
    },

    /**
     * Some magic event subscriber ;)
     * @param event
     * @param callback
     */
    on: function (event, callback) {
        this.events.on(event, function () {
            var args = Array.prototype.slice.call(arguments, 1);
            callback.apply(null, args);
        });
    },


    getMinCellHeight: function (options) {
        var min = options.min ||  this.defaultOptions.min;
        var cellHeight = options.cellHeight ||  this.defaultOptions.cellHeight;
        var cellStep = options.cellStep ||  this.defaultOptions.cellStep;
        return (min / cellStep) * cellHeight;
    },

    _updateOptions: function (options) {
        this.options = $.extend({}, this.defaultOptions, {
            minCellHeight: this.getMinCellHeight(options)
        }, options);
        this._calculateRowsCount();
    },


    _updateWidth: function () {
        this.eachColumns(function (column) {
            column.updateWidth();
        });
    },

    _updateFluid: function () {
        if (this.fluidTimeout) {
            clearTimeout(this.fluidTimeout);
        }

        this.fluidTimeout = setTimeout(function () {
            var diff = this.$el.find('.timeline-time_column').outerWidth();

            var containerWidth = (this.$container.innerWidth() - diff);
            var $columns = this.$columnContiner.children();
            this.options.cellWidth = containerWidth / $columns.length;
            this.eachColumns(function (column) {
                column.updateWidth();
            });
        }.bind(this), 100);
    },

    _initElement: function () {
        this.$el = $('<div class="timeline-table no-select"></div>');
    },

    _initColumnContainer: function () {
        this.$columns = $('<div class="timeline-table-columns"></div>');
        this.$columnContiner = $('<div class="timeline-table-column_container"></div>');
        this.$columns.append(this.$columnContiner);
        this.$el.append(this.$columns);
    },

    _initPointer: function () {
        if (this.options.pointer) {
            this.$pointer = $('<div class="timeline-pointer"></div>');
            this.$pointerTime = $('<div class="timeline-pointer_time"></div>');
            this.$pointer.append(this.$pointerTime);
            this.$el.append(this.$pointer);
        }
    },

    _initTimeColumn: function () {
        var tColumn = new TimeColumn(this.options, this);
        this.columns[tColumn.id] = tColumn;
        this.$columns.prepend(tColumn.$el);
    },

    _initCloneHeader: function (column) {
        var clone = 'data-clone="'+ column.props.clone[0] +'"';
        var cloneHeaderHtml = '';
        if (typeof column.props.headerCloneTemplate == 'function') {
            cloneHeaderHtml = column.props.headerCloneTemplate(column.props.data, column);
        }
        var $header = $('<div class="timeline-column_header_clone_container" '+ clone +'><div class="timeline-column_header_clone">'+ cloneHeaderHtml +'</div><div class="timeline-column_header_clone_content"></div></div>');
        return $header;
    },

    _initCloneColumn: function (column) {
        var clone = 'data-clone="'+ column.props.clone[0] +'"';
        return  $('<div class="timeline-column-clone_container" '+ clone +'></div>');
    },

    _appendClone: function (column) {
        var clone = 'data-clone="'+ column.props.clone[0] +'"';
        var $column = this.$el.find('.timeline-column-clone_container['+ clone +']');
        var $header = this.$header.find('.timeline-column_header_clone_container['+ clone +']');

        if (!$column.length) {
            $header = this._initCloneHeader(column);
            $column = this._initCloneColumn(column);
            this.$columnContiner.append($column);
            this.$header.append($header);
        }
        $column.append(column.$el);
        $header.find('.timeline-column_header_clone_content').append(column.$header);

        var $clones = $column.find('.timeline-column');
        var cellWidth = this.options.cellWidth / $clones.length;

        column.props.cloneOrder = $clones.length;
        $clones.each(function () {
            $(this).width(cellWidth);
        });
    },

    _addColumn: function (column) {
        var id = column.id;
        this.columns[id] = column;

        if (column.props.clone) {
            this._appendClone(column);
        } else {
            this.$columnContiner.append(column.$el);
            this.$header.append(column.$header);
        }
        this.events.trigger('table:add-column', column);
    },

    _initColumns: function () {
        for (var i = 1; i <= this.options.columns; i += 1) {
            this._addColumn(new Column(this.options, this));
        }
    },


    /**
     * Add column to group
     * @param column {Column}
     * @param id {int|string}
     * @private
     */
    _addToGroup: function (column, id) {
        var groupId = id || column.props.group;
        if (groupId) {
            //if array add each
            if ($.isArray(groupId)) {
                groupId.forEach(function (gId) {
                    this._addToGroup(column, gId);
                }.bind(this));
            } else {
                if (!this.groups[groupId]) {
                    this.groups[groupId] = {}
                }
                this.groups[groupId][column.id] = column;
            }
        }

    },

    /**
     * Add new column to table, and append existing cool rows
     * @param props {object|{group: int, background: string, coolRows: [{start: int, end:int}]}}
     * else props may content group id, it's need to sync columns ({group:1})
     * @returns {Column|exports|module.exports}
     */
    addColumn: function (props) {
        var column = new Column(props, this.options, this);
        this._addToGroup(column);
        this._addColumn(column);
        if (props && props.coolRows && props.coolRows.length) {
            props.coolRows.forEach(function (coolRow) {
                column.addCoolRow(coolRow, false);
            });
        }
        return column;
    },

    /**
     * Remove coolRow from collection
     * @param id {string} coolRowId
     */
    removeCoolRow: function (id) {
        this.events.trigger('table:remove-coolRow', id);
    },

    _movePointer: function (e) {
        var diff = $(window).scrollTop() - this.$el.offset().top;
        var top = (e.clientY + diff - 1);
        var time = this.getTimeByPosition(top - this.$header.height() - this.$el.offset().top, this.$columns.height());
        this.$pointer.css({
            top: (top) + 'px'
        });

        this.$pointerTime.html(moment(time).format('HH:mm'))
    },

    _togglePointer: function (e) {
        this.$pointer[e.type == 'mouseleave'? 'hide' : 'show']();
    },

    _bindDomEvents: function () {
        var that = this;
        if (this.options.pointer) {
            this.$el.on('mousemove', this._movePointer.bind(this));
            this.$el.on('mouseenter', this._togglePointer.bind(this));
            this.$el.on('mouseleave', this._togglePointer.bind(this));
        }

        if (this.options.fluid) {
            $(window).on('resize', this._updateFluid.bind(this))
        }

        this.$columnContiner.scroll(function() {
            that.$header.scrollLeft($(this).scrollLeft());
        });
    },


    eachColumns: function (callback) {
        if (typeof callback == 'function') {
            for (var key in this.columns) {
                if (!this.columns[key].props.timecolumn) {
                    callback(this.columns[key])
                }
            }
        }
    },

    getColumn: function (id) {
        return this.columns[id];
    },

    getCoolRow: function (id) {
        var coolRow;
        this.eachColumns(function (column) {
            if (column.coolRows && column.coolRows[id]) {
                coolRow = column.coolRows[id];
            }
        });
        return coolRow;
    },

    _bindEvents: function () {
        this.on('table:save-coolRow', function (row) {
            console.warn('BLOCK HAS BEEN SAVED', row);
        }.bind(this))

        this.on('table:remove-coolRow', function (row) {
            console.warn('BLOCK HAS BEEN REMOVED', row);
        }.bind(this))

        this.on('table:edit-coolRow', function (row) {
            console.warn('BLOCK NEED TO EDIT', row);
        }.bind(this));

        this.on('table:remove-column', this._deleteColumn.bind(this));

        if (this.options.fluid) {
            this.on('table:add-column', this._updateFluid.bind(this));
        } else {
            this.on('table:add-column', this._updateWidth.bind(this));
        }
    },


    _deleteColumn: function (column) {
        delete this.columns[column.id];
    },

    removeColumn: function (id) {
        var column = this.columns[id];
        if (column) {
            column.remove();
            this._updateFluid();
        }
    },

    removeColumns: function () {
        this.eachColumns(function (column) {
            column.remove();
        });
        this._destroy();
    },

    _destroy: function () {
        this.$el.unbind().off();
        this.events.unbind().off();
        this.$el.remove();
    },

    remove: function () {
        this.removeColumns();
        this._destroy();
    }
};

module.exports = TimelineTable;